# Dashboards


В данном проекте собраны дашборды, разработанные в ходе прохождения обучающих курсов "Аналитик данных" и "Симулятор аналитика"

<br>

*Права на задания и учебные материалы принадлежат Karpov Courses (https://karpov.courses/)*

<br>

## Superset 

\* *Superset развернут на сервере Karpov Courses и не позволяет дать прямую ссылку на дэш, обновляемый в реальном времени, поэтому здесь приводятся отдельные скриншоты*

#### 1. Поиск аномалий в данных, рассмотрение различных срезов, фокусировка по временным промежуткам и гео:

![Superset_1_example_Finding_data_loss](Superset_1_example_Finding_data_loss.jpg)

<br>

#### 2. Два графика:
#### а) матрица Retention для когорт пользователей по дате первого контакта с продуктом
#### б) график притока / сохранения / оттока пользователей

![Superset_2_example_Retention_visualisation](Superset_2_example_Retention_visualisation.jpg)

<br>
<br>
<br>

## Redash

\* *Redash также запускается на сервере Karpov Courses, поэтому вместо прямой ссылки на дашборд прилагается скриншот, а также [PDF с детальной инструкцией](Redash_example_dashboard_manual.pdf) по использованию дэша (можно считать её "руководством пользователя", файл Redash_example_dashboard_manual.pdf)*

![Redash_example_dashboard](Redash_example_dashboard.jpg)

<br>
<br>
<br>

## Tableau 

\* *Tableau Public позволяет просматривать дэш по прямой ссылке*

Примеры дашбордов: 

1.  https://public.tableau.com/app/profile/andrew.ionochkin/viz/ionochkin-Dashboard-Homework1/Dashboard

2.  https://public.tableau.com/app/profile/andrew.ionochkin/viz/ionochkin-Dashboard-Homework2/Dashboard

3.  Комплексное задание: 

<blockquote>

а) Интервью с заказчиком, формирование ТЗ на создание дашборда

[Техническое задание](Tableau_3_example_interview_and_specification.pdf), подготовленное по итогам интервью и утверждённое заказчиком

(Файл *Tableau_3_example_interview_and_specification.pdf*)

<br>

б) Сборка дэша согласно ТЗ

https://public.tableau.com/views/ionochkin-pairwisehomework-DashboardCanvas/Dashboard

<br>

в) Доработка по итогам обсуждения с заказчиком

Протокол обсуждения первой версии дашборда + комментарии заказчика:

[Contact report](Tableau_3_example_contact_report.pdf) (Файл *Tableau_3_example_contact_report.pdf*)

</blockquote>